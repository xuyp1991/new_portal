const path = require('path')
const webpack = require('webpack')
const ExtractTextPlugin = require('extract-text-webpack-plugin')
const FriendlyErrorsPlugin = require('friendly-errors-webpack-plugin')
const { VueLoaderPlugin } = require('vue-loader')
const UglifyJsPlugin = require('uglifyjs-webpack-plugin');
const OptimizeCssAssetsPlugin = require('optimize-css-assets-webpack-plugin');


const isProd = process.env.NODE_ENV === 'production'
console.log(`this is ${isProd} from vue-hack-news`)
module.exports = {
  mode: isProd ? 'production' : 'development',
  devtool: isProd
    ? false
    : '#cheap-module-source-map',
  output: {
    path: path.resolve(__dirname, '../dist'),
    publicPath: '/dist/',
    filename: '[name].[chunkhash].js'
  },
  resolve: {
    alias: {
      'public': path.resolve(__dirname, '../public'),
      '@': path.join(__dirname, '../src'),
    }
  },
  optimization: {
    minimizer: [new UglifyJsPlugin()],
  },
  module: {
    noParse: /es6-promise\.js$/, // avoid webpack shimming process
    rules: [
      // {
      //   test: /\.vue$/,
      //   loader: 'vue-loader',
      //   options: {
      //     compilerOptions: {
      //       preserveWhitespace: false
      //     }
      //   }
      // },
      // {
      //   test: /\.css$/i,
      //   use: ['style-loader', 'css-loader'],
      // },
      {
        test: /\.vue$/,
        use: {
          loader: 'vue-loader',
          options: {
            loaders: {
              sass: 'vue-style-loader!style-loader!css-loader!stylus-loader?indentedSyntax=1',
              scss: 'vue-style-loader!style-loader!css-loader!stylus-loader',
              css: 'vue-style-loader!style-loader!css-loader!stylus-loader',
              stylus: 'vue-style-loader!style-loader!css-loader!stylus-loader',
            }
          }
        }
      },
      // {
      //   test: /\.styl(us)?$/,
      //   use: isProd
      //     ? ExtractTextPlugin.extract({
      //         use: [
      //           {
      //             loader: 'css-loader',
      //             options: { minimize: true }
      //           },
      //           'stylus-loader'
      //         ],
      //         fallback: 'vue-style-loader'
      //       })
      //     : ['vue-style-loader', 'style-loader', 'css-loader', 'stylus-loader']
      // },
      {
        test: /.tsx?$/,
        exclude: /node_modules/,
        loader:'ts-loader',    
      },
      {    
        test:/\.(ts|tsx)?$/,
        loader:'ts-loader',    
        options:{
            appendTsSuffixTo:[/\.vue$/],   //为script有lang='ts'标识的脚本文件添加ts后缀
        }
      },
      {
        test: /\.html$/,
        use: 'vue-html-loader'
      },

      {
        test: /\.js$/,
        exclude: /node_modules/,
        use: {
          loader: 'babel-loader',
          options: {
            presets: ['@babel/preset-env'],
            plugins: [
              '@babel/plugin-syntax-dynamic-import',
              '@babel/plugin-transform-runtime'
            ]
          }
        }
      },
      {
        test: /\.(png|jpg|gif|svg)$/,
        loader: 'url-loader',
        options: {
          limit: 1,
          name: '[name].[ext]?[hash]'
        }
      },
      {
        test: /\.css$/,
        use: ExtractTextPlugin.extract({
          fallback: "style-loader",
          use: "css-loader"
        })
      },
      {
        test: /\.styl(us)?$/,
        use: ['vue-style-loader', 'style-loader', 'css-loader', 'stylus-loader']
      },
      // {
      //   test: /\.style$/,
      //   use: isProd
      //     ? ExtractTextPlugin.extract({
      //         use: [
      //           {
      //             loader: 'css-loader',
      //             options: { minimize: true }
      //           },
      //           'stylus-loader'
      //         ],
      //         fallback: 'vue-style-loader'
      //       })
      //     : ['vue-style-loader', 'style-loader', 'css-loader', 'stylus-loader']
      // },
      
    ]
  },
  performance: {
    hints: false
  },
  // optimization: {
  //   runtimeChunk: {
  //       name: "manifest"
  //   },
  //   splitChunks: {
  //       cacheGroups: {
  //           commons: {
  //               test: /[\\/]node_modules[\\/]/,
  //               name: "vendor",
  //               chunks: "all"
  //           }
  //       }
  //   }
  // },
  plugins: isProd
    ? [
        new VueLoaderPlugin(),
        // new webpack.optimize.UglifyJsPlugin({
        //   compress: { warnings: false }
        // }),
        new webpack.optimize.ModuleConcatenationPlugin(),
        new ExtractTextPlugin({
          filename: 'common.[chunkhash].css'
        }),
        new OptimizeCssAssetsPlugin({
          assetNameRegExp: /\.css$/g,
          cssProcessor: require('cssnano'),
          cssProcessorPluginOptions: {
            preset: ['default', { discardComments: { removeAll: true } }],
          },
          canPrint: true
        })
      ]
    : [
        new ExtractTextPlugin({
          filename: 'common.[chunkhash].css'
        }),
        new VueLoaderPlugin(),
        new FriendlyErrorsPlugin()
      ]
}

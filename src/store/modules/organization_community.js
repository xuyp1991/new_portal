
import {
  query_news_by_tags as QUERY_ORGANIZATION_RULLS,
  query_news_by_id,
} from '@/service'

import {
  NEWS_STATIC_HOST as plans_static_host
} from '@/service/index.js'

const OPTIONS = {
  UPDATE_ORGANIZATION_RULLS: 'UPDATE_ORGANIZATION_RULLS',

  CLEAR_ORGANIZATION_RULLS: 'CLEAR_ORGANIZATION_RULLS',

  UPDATE_ORGANIZATION_RULLS_PAGE:  'UPDATE_ORGANIZATION_RULLS_PAGE',

  UPDATE_ORGANIZATION_RULLS_TOTAL: 'UPDATE_ORGANIZATION_RULLS_TOTAL',

  UPDATE_ORGANIZATION_RULLS_LIMIT: 'UPDATE_ORGANIZATION_RULLS_LIMIT',

  UPDATE_ORGANIZATION_RULLS_LOAD_STAUS: 'UPDATE_ORGANIZATION_RULLS_LOAD_STAUS',

  UPDATE_IMG_URL: 'update_img_url',

  START_LOAD: 'start_load',

  END_LOAD: 'end_load',

  UPDATE_ORGANIZATION_RULLS_INFO: 'UPDATE_ORGANIZATION_RULLS_INFO',

  LOAD_ORGANIZATION_RULLS: 'LOAD_ORGANIZATION_RULLS',

  LOAD_FIRST_PAGE_ORGANIZATION_RULLS: 'LOAD_FIRST_PAGE_ORGANIZATION_RULLS',

  LOAD_NEXT_PAGE_ORGANIZATION_RULLS: 'LOAD_NEXT_PAGE_ORGANIZATION_RULLS',

  LOAD_ORGANIZATION_RULLS_BY_ID: 'LOAD_ORGANIZATION_RULLS_BY_ID',

  GET_ORGANIZATION_RULLS: 'GET_ORGANIZATION_RULLS'
}

const initState = {
  org_rulls: [],
  page: 1,
  limit: 10,
  total: 0,
  more: true,
  on_load: true,
  error_msg: ''
}

const mutations = {
  [OPTIONS.UPDATE_ORGANIZATION_RULLS] (state, plans) {
    state.org_rulls.splice(state.org_rulls.length, 0, ...plans);
  },
  [OPTIONS.CLEAR_ORGANIZATION_RULLS] (state) {
    state.page = 1;
    state.total = 0;
    state.org_rulls.splice(0, state.org_rulls.length);
  },
  [OPTIONS.UPDATE_IMG_URL] (state) {
    let data = state.org_rulls;
    for(let plans_item of data){
      let val = [];
      try{
        val = JSON.parse( plans_item.description )
      }catch(e){
        val = [
           {'insert': plans_item.description}
         ]
      }

      if(!val.splice){
        val = [
           {'insert': plans_item.description}
         ]
      }

      for(let row of val){
        if(row.insert && row.insert.image){
          let is_url = row.insert.image.indexOf('http://') == 0 || row.insert.image.indexOf('httpw://') == 0
          if(!is_url){
            row.insert.image = `${plans_static_host}${row.insert.image}`;
          }
        }
      }

      plans_item.description = JSON.stringify(val);
    }

    for(let row of data){
      row.sliders.forEach((i, index) => {
        let base_file_name = i.split('/static/')[1];
        if(base_file_name){
          row.sliders[index] = `${ plans_static_host }/static/${ base_file_name }`
        }
      });
    }
  },
  [OPTIONS.UPDATE_ORGANIZATION_RULLS_LIMIT] (state, limit) {
    state.limit = limit
  },
  [OPTIONS.UPDATE_ORGANIZATION_RULLS_TOTAL] (state, total) {
    state.total = total;
  },
  [OPTIONS.UPDATE_ORGANIZATION_RULLS_PAGE] (state, page) {
    state.page = page;
  },
  [OPTIONS.UPDATE_ORGANIZATION_RULLS_LOAD_STAUS] (state, status = false) {
    state.on_load = status;
  },
  [OPTIONS.START_LOAD] (state) {
    state.on_load = true;
  },
  [OPTIONS.END_LOAD] (state) {
    state.on_load = false;
  },
  [OPTIONS.UPDATE_ORGANIZATION_RULLS_INFO] (state, {page, limit, total}) {
    state.page = page;
    state.limit = limit;
    state.total = total;
    if (state.page >= total) {
      state.more = false;
    } else {
      state.more = true;
    }
  }
}

const actions = {
  async [OPTIONS.LOAD_ORGANIZATION_RULLS] ({state, commit}, params = {tags: []}) {
    commit(OPTIONS.START_LOAD);
    params.tag = 'EOSC社区自治组织'
    let {is_error, data: base_data} = await QUERY_ORGANIZATION_RULLS({
      page: state.page, 
      tags: params.tags,
      tag: params.tag,
      limit: 3
    });
    if(is_error){
      commit(OPTIONS.END_LOAD);
      return ;
    }
    const {page, limit, total, data} = base_data;

    // const plans_static_host = 'http://127.0.0.1:1213'

    commit(OPTIONS.UPDATE_ORGANIZATION_RULLS, data);
    commit(OPTIONS.UPDATE_IMG_URL);
    commit(OPTIONS.UPDATE_ORGANIZATION_RULLS_INFO, {page, limit, total});

    commit(OPTIONS.END_LOAD);
    return data;
  },
  async [OPTIONS.LOAD_NEXT_PAGE_ORGANIZATION_RULLS] ({state, commit, dispatch}, params = {tags: []}) {
    let page  = state.page,
        total = state.total;

    if(!state.more) return;

    if(page >= total) return;

    let next_page = page + 1;

    commit(OPTIONS.UPDATE_ORGANIZATION_RULLS_PAGE, next_page);
    await dispatch(OPTIONS.LOAD_ORGANIZATION_RULLS, params);

  },
  async [OPTIONS.LOAD_FIRST_PAGE_ORGANIZATION_RULLS] ({state, commit, dispatch}, params = {tags: []}) {
    commit(OPTIONS.CLEAR_ORGANIZATION_RULLS);
    console.log('LOAD_FIRST_PAGE_ORGANIZATION_RULLS')
    await dispatch(OPTIONS.LOAD_ORGANIZATION_RULLS, params);
  },
  async [OPTIONS.LOAD_ORGANIZATION_RULLS_BY_ID] ({state, commit, dispatch}, plans_id) {
    let {is_error, data: base_data} = await query_news_by_id({id: plans_id});
    commit(OPTIONS.CLEAR_ORGANIZATION_RULLS);
    commit(OPTIONS.UPDATE_ORGANIZATION_RULLS, base_data);
    commit(OPTIONS.UPDATE_IMG_URL);
  }
}

const getters = {

}

export default {
    state: initState,
    mutations,
    actions,
    getters,
};
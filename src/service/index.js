import axios from 'axios'

const explorer_host = 'https://explorer.eosforce.io/'
const chain_host = 'https://w3.eosforce.cn/'

export const NEWS_HOST = 'https://shuyuchain.com/news_back/';
export const NEWS_STATIC_HOST = 'https://shuyuchain.com/';

// export const NEWS_HOST = 'http://0.0.0.0:1213/news_back/';
// export const NEWS_STATIC_HOST = 'http://0.0.0.0:1213/';

const BASE_HEADERS = {
  'Acces-Control-Allow-Origin': '*',
  'accept': 'application/json',
  'content-type': 'application/json',
  'mode': 'cors'
}

const request = async ({url = '', data , method = 'POST'}) => {
  let params = {
    cache: 'no-cache', 
    method,
    mode: 'cors', 
    redirect: 'follow', 
    referrer: 'no-referrer', 
  }

  try{
    let body = data instanceof FormData ? data : JSON.stringify(data);
    console.log(data, url);
    params.body = body;
  }catch(e){

  }

  let res = await fetch(url, params)
              .then(response => {
                return response.json();
              });
  return res;
}

export const POST = async (params, url) => {
  let res = await axios.post(url, params, BASE_HEADERS).catch(error => {
    return {
      data: {
        is_error: true,
        msg: error,
        data: []
      }
    }
  });
  let {is_error, msg} = res.data;
  return res.data;
}

export const check_analytics = async () => {
  const url = explorer_host + 'web/get_analytics';
  return await request({url });
}

export const get_info = async () => {
  const url = chain_host + 'v1/chain/get_info';
  return await request({url, method: 'GET'})
}

export const get_table_rows = async (params = {}) => {
  const url = chain_host + 'v1/chain/get_table_rows';
  return await request({url, data: params});
}

const wait_time = async (_time) => {
  return new Promise((rs, rj) => {
    setTimeout(() => {
      rs();
    }, _time * 1000);
  });
}

export const query_for_form = async (params = {}) => {
  let values = [], file_names = [];
  while(1){
    let els = [], el_len = 2;
    for(let _index of new Array(el_len).keys()){
      let ipt_id = `file_test${_index}`;
      let el = document.getElementById(ipt_id);
      if(!el){
        break;
      }
      els.push(el);
      // console.log(ipt_id, el.value);
      if(el.files[0]){
        values.push(el.files[0]);  
      }
      
      if(el.value){
        file_names.push(el.value)  
      }
      
      // console.log('ipt_id');
      // console.log(ipt_id);
    }
    await wait_time(2);
    // console.log('test')
    if(values.length == el_len){
      break ;
    }
    values = [];
    file_names = [];
  }

  // console.log(values);
  // console.log(file_names);
  let send_params = new FormData();
  send_params.append('name', 'cool');
  send_params.append('age', 10);
  send_params.append('sex', 1);
  for(let _index of values.keys()){
    send_params.append(`files[${_index}]`, values[_index], file_names[_index]);
  }
  
  // send_params = {
  //   name: 'cool',
  //   age: 10,
  // }
  params = {
    url: 'http://0.0.0.0:8000/block/view_post_from_form_data.do',
    data: send_params,
    method: 'POST'
  }
  let result = await  request(params);
  console.log(result);
  await wait_time(10);
  query_for_form();
}

export const add_code_to_chain = async (code) => {
  let params = {
    url: '',
    data: {
      code: 'te'
    },
    method: 'POST'
  }
  let result = await request(params);
  return result;
}

export * from './news'
export * from './plans'
